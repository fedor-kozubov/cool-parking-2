﻿using System;
using CoolParking.BL.Models;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.BL.ConsoleApp
{
    class Program
    {

        private enum HttpMethod
        {
            Get,
            Post,
            Delete,
            Put
        }

        private static void HttpProcess(HttpMethod method, string url, StringContent stringContent, out HttpStatusCode httpStatusCode, out string responseBody)
        {
            Task<HttpResponseMessage> response;

            using (var client = new HttpClient())
            {
                try
                {
                    switch (method)
                    {
                        case HttpMethod.Get:
                            response = client.GetAsync(url);
                            break;

                        case HttpMethod.Post:
                            response = client.PostAsync(url, stringContent);
                            break;

                        case HttpMethod.Delete:
                            response = client.DeleteAsync(url);
                            break;

                        case HttpMethod.Put:
                            response = client.PutAsync(url, stringContent);
                            break;

                        default:
                            httpStatusCode = HttpStatusCode.NotImplemented;
                            responseBody = "";
                            return;
                    }
                    httpStatusCode = response.Result.StatusCode;
                    responseBody = response.Result.Content.ReadAsStringAsync().Result;
                }
                catch (Exception ex)
                {
                    httpStatusCode = HttpStatusCode.NotImplemented;
                    responseBody = ex.Message;
                }
            }
        }

        static void Main(string[] args)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            const string HostName = "http://localhost:25177";

            HttpStatusCode httpStatusCode;
            string responseBody;

            /*
            Вывести на экран текущий баланс Парковки.
            Вывести на экран сумму заработанных денег за текущий период(до записи в лог).
            Вывести на экран количество свободных / занятых мест на парковке.
            Вывести на экран все Транзакции Парковки за текущий период(до записи в лог).
            Вывести историю транзакций(считав данные из файла Transactions.log).
            Вывести на экран список Тр.средств находящихся на Паркинге.
            Поставить Тр. средство на Паркинг.
            Забрать транспортное средство с Паркинга.
            Пополнить баланс конкретного Тр. средства.
            */

            const string CommandsList = "balance\r\n" +
                                        //"lastprofit\r\n" +
                                        "places\r\n" +
                                        "lasttransactions\r\n" +
                                        "readlog\r\n" +
                                        "vehicles\r\n" +
                                        "addvehicle id sum type[passengercar | truck | bus | motorcycle]\r\n" +
                                        "removevehicle id\r\n" +
                                        "topupvehicle id sum\r\n" +
                                        "help\r\n" +
                                        "exit";

            Console.WriteLine("Welcome to CoolParking");
            Console.WriteLine("commands list:");
            Console.WriteLine(CommandsList);

            while (true)
            {
                // syntax: command id sum type
                string[] input = Console.ReadLine().Split(new char[] { ' ' });
                if (input.Length == 0) continue;

                string command = input[0].ToLower();

                string? id = (input.Length > 1) ? input[1] : null;

                decimal? sum = null;
                if (input.Length > 2)
                {
                    if (Decimal.TryParse(input[2], out decimal sum2))
                        sum = sum2;
                }

                VehicleType? vehicleType = null;
                if (input.Length > 3)
                {
                    switch (input[3].ToLower())
                    {
                        case "passengercar":
                            vehicleType = VehicleType.PassengerCar;
                            break;
                        case "truck":
                            vehicleType = VehicleType.Truck;
                            break;
                        case "bus":
                            vehicleType = VehicleType.Bus;
                            break;
                        case "motorcycle":
                            vehicleType = VehicleType.Motorcycle;
                            break;
                        default:
                            break;
                    }
                }


                switch (command)
                {
                    case "balance":
                        HttpProcess(HttpMethod.Get, $"{HostName}/api/Parking/balance", null, out httpStatusCode, out responseBody);
                        Console.WriteLine(responseBody);
                        break;

                    //case "lastprofit":
                    //    break;

                    case "places":
                        HttpProcess(HttpMethod.Get, $"{HostName}/api/Parking/freePlaces", null, out httpStatusCode, out responseBody);
                        Console.WriteLine(responseBody);
                        break;

                    case "lasttransactions":
                        HttpProcess(HttpMethod.Get, $"{HostName}/api/transactions/last", null, out httpStatusCode, out responseBody);
                        Console.WriteLine(responseBody);
                        break;

                    case "readlog":
                        HttpProcess(HttpMethod.Get, $"{HostName}/api/transactions/all", null, out httpStatusCode, out responseBody);
                        Console.WriteLine(responseBody);
                        break;

                    case "vehicles":
                        HttpProcess(HttpMethod.Get, $"{HostName}/api/vehicles", null, out httpStatusCode, out responseBody);
                        Console.WriteLine(responseBody);
                        break;

                    case "addvehicle":
                        if (id == null || sum == null || vehicleType == null)
                        {
                            Console.WriteLine("Wrong format");
                            break;
                        }
                        string postJson = @$"{{ ""Id"": ""{id}"",""VehicleType"": {(int)vehicleType},""Balance"": {sum}}}";
                        StringContent postContent = new StringContent(postJson, Encoding.UTF8, "application/json");
                        HttpProcess(HttpMethod.Post, $"{HostName}/api/vehicles", postContent, out httpStatusCode, out responseBody);
                        Console.WriteLine(httpStatusCode == HttpStatusCode.Created ? "Ok." : responseBody);
                        break;

                    case "removevehicle":
                        if (id == null)
                        {
                            Console.WriteLine("Wrong format");
                            break;
                        }
                        HttpProcess(HttpMethod.Delete, $"{HostName}/api/vehicles/{id}", null, out httpStatusCode, out responseBody);
                        Console.WriteLine(httpStatusCode == HttpStatusCode.NoContent ? "Ok." : responseBody);
                        break;

                    case "topupvehicle":
                        if (id == null || sum == null)
                        {
                            Console.WriteLine("Wrong format");
                            break;
                        }
                        //string putJson = @$"{{ ""id"": ""{id}"",""Sum"": {sum}}}";
                        string putJson = @$"{{ ""Id"": ""{id}"",""Sum"": {sum}}}";
                        StringContent putContent = new StringContent(putJson, Encoding.UTF8, "application/json");
                        HttpProcess(HttpMethod.Put, $"{HostName}/api/transactions/topUpVehicle", putContent, out httpStatusCode, out responseBody);
                        Console.WriteLine(httpStatusCode == HttpStatusCode.OK ? "Ok." : responseBody);
                        break;

                    case "help":
                        Console.WriteLine(CommandsList);
                        break;

                    case "":
                        break;

                    case "exit":
                        return;

                    default:
                        Console.WriteLine($"Unknown command {input[0]}");
                        break;
                }
            }
        }
    }
}
