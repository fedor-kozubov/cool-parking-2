using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.IO;
using System.Reflection;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        public static ParkingService parkingService = null;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            string logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

            ITimerService withdrawTimer = new TimerService();
            withdrawTimer.Interval = Settings.WithdrawInterval * 1000;

            ITimerService logTimer = new TimerService();
            logTimer.Interval = Settings.LogInterval * 1000;

            ILogService logService = new LogService(logFilePath);
            parkingService = new ParkingService(withdrawTimer, logTimer, logService);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
