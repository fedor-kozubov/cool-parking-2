﻿using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.DataObjects
{
    public class TopUpVehicleRequest
    {
        //[JsonPropertyName("id")]
        [JsonPropertyName("Id")]
        public string Id { get; set; }

        [JsonPropertyName("Sum")]
        public decimal Sum { get; set; }
    }
}
