﻿using System;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Models;
using System.Text.Json;
using System.Collections.ObjectModel;
using System.IO;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {

        [HttpGet]
        public ActionResult<ReadOnlyCollection<Vehicle>> Get()
        {
            return Ok(Startup.parkingService.GetVehicles());
        }

        [HttpGet("{id}")]
        public ActionResult<string> Get(string id)
        {
            if (!Vehicle.IdIsValid(id))
            {
                return BadRequest("Id is invalid");
            }

            Vehicle vehicle = Startup.parkingService.GetVehicleById(id);
            if (vehicle == null)
            {
                return NotFound("Vehicle not found");
            }
            else
            {
                return Ok(JsonSerializer.Serialize<Vehicle>(vehicle));
            }
        }

        /*
        // Not catch Deserialize exseptions!
        // 
        [HttpPost]
        [Consumes("application/json")]
        public ActionResult<string> Post(Vehicle vehicle)
        {
            try
            {
                Startup.parkingService.AddVehicle(vehicle);
                return Created("", JsonSerializer.Serialize<Vehicle>(vehicle));
            }
            catch (ArgumentException ex) // Already existing vehicle Id
            {
                return Conflict(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        */

        [HttpPost]
        public ActionResult<string> PostIgnoreContentTypeHeader()
        {
            using (var reader = new StreamReader(HttpContext.Request.Body))
            {
                string body = reader.ReadToEndAsync().Result;
                try
                {
                    Vehicle vehicle = JsonSerializer.Deserialize<Vehicle>(body);
                    Startup.parkingService.AddVehicle(vehicle);
                    return Created("", JsonSerializer.Serialize<Vehicle>(vehicle));
                }
                catch (ArgumentException ex) // Already existing vehicle Id
                {
                    return BadRequest(ex.Message);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
        }


        [HttpDelete("{id}")]
        public ActionResult<string> Delete(string id)
        {
            if (!Vehicle.IdIsValid(id))
            {
                return BadRequest("Id is invalid");
            }

            try
            {
                Startup.parkingService.RemoveVehicle(id);
            }
            catch (ArgumentException ex) // Attempt to remove unexisting vehicle
            {
                return NotFound(ex.Message);
            }
            catch (InvalidOperationException ex) // "Attempt to remove vehicle with negative balance"
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }

    }
}
