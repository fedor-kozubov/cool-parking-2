﻿using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {

        [HttpGet("balance")]
        public ActionResult<decimal> GetParkingBalance()
        {
            return Ok(Startup.parkingService.GetBalance());
        }

        [HttpGet("capacity")]
        public ActionResult<int> GetParkingCapacity()
        {
            return Ok(Startup.parkingService.GetCapacity());
        }

        [HttpGet("freeplaces")]
        public ActionResult<int> GetParkingFreePlaces()
        {
            return Ok(Startup.parkingService.GetFreePlaces());
        }

    }
}