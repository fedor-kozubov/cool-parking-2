﻿using System;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DataObjects;
using System.Text.Json;
using System.IO;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {

        [HttpGet("last")]
        public ActionResult<string> GetLastTransactions()
        {
            TransactionInfo[] transactions = Startup.parkingService.GetLastParkingTransactions();

            //if (transactions.Length == 0)
            //    return NoContent();

            return Ok(JsonSerializer.Serialize<TransactionInfo[]>(transactions));
        }

        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                return Ok(Startup.parkingService.LogService.Read());
            }
            catch (InvalidOperationException) // File not found
            {
                return NotFound("File not found");
            }
            catch (Exception)
            {
                return Problem("", "", 500, "", ""); // 500 Internal Server Error
            }
        }

        /*
        [HttpPut("topUpVehicle")]
        public ActionResult<string> Put(TopUpVehicleRequest topUpVehicleRequest)
        {
            Vehicle vehicle = Startup.parkingService.GetVehicleById(topUpVehicleRequest.Id);
            if (vehicle == null)
            {
                return BadRequest("Vehicle not found or wrong request body");
            }

            try
            {
                Startup.parkingService.TopUpVehicle(vehicle, topUpVehicleRequest.Sum);
            }
            catch (ArgumentException ex) // Negative sum
            {
                return BadRequest(ex.Message);
            }

            return Ok(JsonSerializer.Serialize<Vehicle>(vehicle));
        }
        */

        [HttpPut("topUpVehicle")]
        public ActionResult<string> Put()
        {
            using (var reader = new StreamReader(HttpContext.Request.Body))
            {
                string body = reader.ReadToEndAsync().Result;
                try
                {
                    TopUpVehicleRequest topUpVehicleRequest = JsonSerializer.Deserialize<TopUpVehicleRequest>(body);
                    if(topUpVehicleRequest.Id == null)
                    {
                        return BadRequest("Wrong request body");                        
                    }
                    if(!Vehicle.IdIsValid(topUpVehicleRequest.Id))
                    {
                        return BadRequest("Wrong Id");
                    }
                    Vehicle vehicle = Startup.parkingService.GetVehicleById(topUpVehicleRequest.Id);
                    if (vehicle == null)
                    {
                        return NotFound("Vehicle not found");
                    }
                    Startup.parkingService.TopUpVehicle(vehicle, topUpVehicleRequest.Sum);
                    return Ok(JsonSerializer.Serialize<Vehicle>(vehicle));
                }
                catch (ArgumentException ex) // Negative sum
                {
                    return BadRequest(ex.Message);
                }
                catch (Exception ex) // Deserialize error
                {
                    return BadRequest(ex.Message);
                }
            }
        }

    }
}
