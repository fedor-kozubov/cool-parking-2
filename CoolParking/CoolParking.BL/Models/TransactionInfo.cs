﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime Time { get; set; }
        public string Id { get; set; }
        public decimal Sum { get; set; }

        public TransactionInfo(string id, decimal sum)
        {
            Time = DateTime.Now;
            Id = id;
            Sum = sum;
        }

        public override string ToString()
        {
            return $"{Time}\t{Id}\t{Sum}";
        }

    }
}